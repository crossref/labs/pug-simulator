import pluggy

hookimpl = pluggy.HookimplMarker("pugdog")


class Sleep:
    @hookimpl
    def about(self):
        return "About the 'Sleep' plugin: Pugs are lazy!"

    @hookimpl
    def morning(self, name):
        return f"{name} is awake!"

    @hookimpl
    def noon(self, name):
        return f"{name} has a nap."

    @hookimpl
    def evening(self, name):
        return f"{name} is drowsy."

    @hookimpl
    def night(self, name):
        return f"{name} says ZZzzzzzzzzz"
