import importlib
import pluggy
from pug_simulator.hookspec import PugHooks

pm = pluggy.PluginManager("pugdog")
pm.add_hookspecs(PugHooks)


def test_about():
    expected = "About the 'Eat' plugin: PUGS LOVE FOOD!"
    module = importlib.import_module("plugins_enabled.eat.eat")
    plugin = getattr(module, "Eat")
    pm.register(plugin)
    result = pm.hook.about()
    assert result == [expected]
