import pluggy
from pug_simulator.pug_base_plugin import PugPlugin

hookimpl = pluggy.HookimplMarker("pugdog")


class Eat(PugPlugin):
    @hookimpl
    def about(self):
        return "About the 'Eat' plugin: PUGS LOVE FOOD!"

    @hookimpl
    def morning(self, name):
        return f"{name} ate breakfast"

    @hookimpl
    def noon(self, name):
        return f"The pug is hungry. {name} is glaring at you."

    @hookimpl
    def evening(self, name):
        return f"{name} ate dinner"
