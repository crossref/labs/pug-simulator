import typer
import importlib
from pathlib import Path
from glob import glob
import pluggy
import os

from pug_simulator.hookspec import PugHooks

PLUGINS_ENABLED_DIR = "plugins_enabled"

pm = pluggy.PluginManager("pugdog")
pm.add_hookspecs(PugHooks)


def enabled_plugin_paths():
    """Return a list of paths to enabled plugins
    NB that we use glob to find all python files in the plugins_enabled directory
    and its subdirectories because glob works with symlinks and we want to be able to
    use symlinks to enable and disable plugins"""
    return [
        Path(filename)
        for filename in glob(f"{PLUGINS_ENABLED_DIR}/**/*.py", recursive=True)
        if "__" not in filename and "tests" not in filename
    ]


def path_to_module_name(path: Path) -> str:
    """Convert a path to a module name"""
    return str(path).replace(os.path.sep, ".").replace(".py", "")


def load_plugins():
    # list python files in the dynamic_plugins directory
    # import them, and register them with the plugin manager
    # NB: this assumes that the plugin class is named the same as the file
    # e.g. eat.py contains a class called Eat
    for plugin_path in enabled_plugin_paths():
        module_name = path_to_module_name(plugin_path)
        module = importlib.import_module(module_name)
        class_name = plugin_path.stem.title()
        plugin = getattr(module, class_name)
        pm.register(plugin())


def simulate_pug(
    name: str = typer.Option(..., prompt=True, help="Name of the pug to simulate"),
    about: bool = typer.Option(
        False, "--about", help="Show information about the plugins"
    ),
):
    load_plugins()

    if about:
        print("About the pug simulator plugins\n\n")
        for result in pm.hook.about():
            print(result)
            print("-" * 20)

    print(f"Simulating a pug called {name}")
    print("It is morning")
    print(pm.hook.morning(name=name))
    print(pm.hook.anytime(name=name))
    print("It is noon")
    print(pm.hook.noon(name=name))
    print(pm.hook.anytime(name=name))
    print("It is evening")
    print(pm.hook.evening(name=name))
    print("It is night")
    print(pm.hook.night(name=name))


def cli():
    typer.run(simulate_pug)


if __name__ == "__main__":
    typer.run(simulate_pug)
