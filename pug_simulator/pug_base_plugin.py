import pluggy
import random

hookimpl = pluggy.HookimplMarker("pugdog")


class PugPlugin:
    def snort(self):
        return random.choice(["snort", "snort, snort", "snort, snort, snort"])

    @hookimpl
    def about(self):
        return "There is no 'about' text for this pug plugin"

    @hookimpl
    def anytime(self, name):
        return f"{name} goes: {self.snort()}"
