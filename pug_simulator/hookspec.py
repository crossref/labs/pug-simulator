import pluggy

hookspec = pluggy.HookspecMarker("pugdog")


class PugHooks:
    @hookspec
    def about(self):
        "about hook"
        pass

    @hookspec
    def anytime(self, name):
        "anytime hook"
        pass

    @hookspec
    def morning(self, name):
        "morning hook"
        pass

    @hookspec
    def noon(self, name):
        "noon hook"
        pass

    @hookspec
    def evening(self, name):
        "evening hook"
        pass

    @hookspec
    def night(self, name):
        "night hook"
        pass
