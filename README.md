# Pug

A pug simulation.

## To install

- `git clone git@gitlab.com:crossref/labs/pug-simulator.git`
- `cd pug-simulator`
- `python -m venv venv`
- `source ./venv/bin/activate`
- `pip install -e .`

# And if you plan on developing

- `pip install -e '.[dev]'`

The repo includes a `Justfile` for managing development tasks with [Just](https://github.com/casey/just).

To install `just` on MacOS:

- `brew install just`

To install `just` on Linux:

- `sudo apt install just`


Once `just` is installed, you can run things like:

- `just test` to run tests
- `just lint` to check code style and formatting
- `just fix` to auto-fix linting issues

## Explanation

Pug implements a simulation of a pug. It is designed to illustrate a plugin architecture using `pluggy.`
The behavior of the pug throughout the day can be modified by adding plugins (aka "pugins") that implement one of several `hooks`:

- about
- anytime
- morning
- noon
- evening
- night

These are specified by `hookspec.py`

There is also a default `PugPlugin` class from which other plugins can inherit (`pug_simulator/pug_plugin.py)`). This is designed to show that, by using class-based plugins, you can create a set of utility functions that are available to all pug plugins

There is a CLI tool called `pug`. It will load behaviors from the `plugins_enabled` directory and then execute each of the above "hooks".

The plugins are all in the `plugins_enabled` directory.


## Running the pug simulation

```
Usage: pug.py [OPTIONS]                                              
                                                                      
╭─ Options ──────────────────────────────────────────────────────────╮
│ *  --name         TEXT  Name of the pug to simulate                │
│                         [default: None]                            │
│                         [required]                                 │
│    --about              Show information about the plugins         │
│    --help               Show this message and exit.                │
╰────────────────────────────────────────────────────────────────────╯
```

## Example output

```
$ pug
Name: Loretta
Simulating a pug called Loretta
It is morning
['Loretta ate breakfast', 'Loretta is awake!']
['Loretta goes: snort']
It is noon
['The pug is hungry. Loretta is glaring at you.', 'Loretta has a nap.']
['Loretta goes: snort']
It is evening
['Loretta ate dinner', 'Loretta is drowsy.']
It is night
['Loretta says ZZzzzzzzzzz']
```








