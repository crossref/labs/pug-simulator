def test_path_to_module_name():
    from pathlib import Path
    from pug_simulator.cli import path_to_module_name

    assert path_to_module_name(Path("foo/bar/baz.py")) == "foo.bar.baz"
